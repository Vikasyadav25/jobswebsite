import { connect } from 'react-redux';
import { getPost } from '../../store/selectors/PostSelectors';
import {FaUserCircle} from 'react-icons/fa';
function SinglePost(props) {
    return (
      <div>
        <div>Single Post Page</div>
        <div>Id: {props.post.id}</div>
        <div>Job Title:{props.post.title}</div>
        <div><FaUserCircle / ></div>
        <div>Company Name: {props.post.company} </div>
        <div>Salary : {props.post.salary} </div>
        <div>Date Time: {props.post.datetime} </div>
        <div>Description: {props.post.description}</div>
      </div>
    );
}

const makeStateToProps = () => {
    const post = getPost();
    return (state, props) => {
        return {
            post: post(state, props.match.params.id),
        };
    };
};

export default connect(makeStateToProps)(SinglePost);
